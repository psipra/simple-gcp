terraform {
    backend "gcs" {
         credentials = "noted-gift.json"
         bucket      = "sample__buckets"
    }
}
provider "google" {
  credentials = file("noted-gift.json")
  project = "noted-gift-336510"
  region  = "europe-west2"
  
}

resource "google_compute_network" "vpc" {
 name                    = "dev-vpc"
 auto_create_subnetworks = "false"
}


resource "google_compute_subnetwork" "subnet" {
 name          = "dev-subnet"
 ip_cidr_range = "10.10.0.0/24"
 network       = "dev-vpc"
 depends_on    = [google_compute_network.vpc]
 region      = "europe-west2"
}

resource "google_compute_firewall" "firewall" {
  name    = "dev-firewall"
  network = "${google_compute_network.vpc.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}